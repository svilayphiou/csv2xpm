# csv2xpm

Translates a CSV file into an XPM file.
Used to make a black and white pattern, based on odd/even numbers, in LibreOffice and transform it into a picture via the XPM format. 

## Usage

In a terminal, simply call the script and your filename:

    python csv2xpm.py my_pattern.csv
