import sys

i = sys.argv[1]

f = open(i, "r")
lines = f.readlines()
f.close()

bbb = ""

for line in lines:
    l = line.split(";")
    for nb in l:
        try: 
            mod = int(nb) % 2
            if mod == 0:
                bbb += "."
            else:
                bbb += "0"
        except: 
            pass
    bbb += "\n"
print(bbb)

width = len(lines[0].split(";"))
height = len(lines)

# width height colors char/px
header = """! XPM2
%d %s 2 1
0 c #000000
. c #ffffff
""" % (width, height)

o = i.split(".")[0] + ".xpm"
f = open(o, "w")
f.write(header)
f.write(bbb)
f.close()
